import sys
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline
from sklearn.ensemble import RandomForestRegressor
from sklearn.preprocessing import  StandardScaler
from sklearn.metrics import mean_squared_error, r2_score
from sklearn.decomposition import PCA
def main(path_train, path_validate = None):
	df_train = pd.read_csv(path_train)
	df_train = pd.get_dummies(df_train)
	X_train, X_test, Y_train, Y_test = train_test_split(df_train.drop(['Age'], axis=1).values, df_train['Age'].values, test_size = 0.2, random_state=5)

	print('Number of traing data:', X_train.shape[0])
	print('Number of testing data:', X_test.shape[0])

	pipe = Pipeline([('scl', StandardScaler()),
                  ('pca', PCA(n_components=10)),
                  ('rfr', RandomForestRegressor(max_features=7, max_depth=10, n_estimators=200))])
	pipe.fit(X_train, Y_train)

	# model evaluation for training set
	y_train_predict = pipe.predict(X_train)
	rmse = (np.sqrt(mean_squared_error(Y_train, y_train_predict)))
	r2 = r2_score(Y_train, y_train_predict)

	print("Random Forest Regression performance for training set")
	print("--------------------------------------")
	print('RMSE is {}'.format(rmse))
	print('R2 score is {}'.format(r2))
	print("\n")

	# model evaluation for testing set
	y_test_predict = pipe.predict(X_test)
	rmse = (np.sqrt(mean_squared_error(Y_test, y_test_predict)))
	r2 = r2_score(Y_test, y_test_predict)

	print("Random Forest Regression performance for testing set")
	print("--------------------------------------")
	print('RMSE is {}'.format(rmse))
	print('R2 score is {}'.format(r2))

	print("\n\n")	

	if path_validate != None:
		df_validate_raw = pd.read_csv(path_validate)
		df_validate = pd.get_dummies(df_validate_raw)

		X_validate = df_validate.drop(['Age'], axis=1).values
		Y_validate = df_validate['Age'].values

		# model evaluation for evaluation set
		y_validate_predict = pipe.predict(X_validate)
		rmse = (np.sqrt(mean_squared_error(Y_validate, y_validate_predict)))
		r2 = r2_score(Y_validate, y_validate_predict)

		print("Random Forest Regression performance for validation set")
		print("--------------------------------------")
		print('RMSE is {}'.format(rmse))
		print('R2 score is {}'.format(r2))		
		print("\n")	

		y_validate_predict_rounded = np.rint(y_validate_predict)
		df_validate_raw['Predicted Age'] = y_validate_predict_rounded
		df_validate_raw.to_csv('output.csv')
		# print(df_validate_raw.head())

if __name__ == "__main__":
  if len(sys.argv) == 2 :
  	main(sys.argv[1])
  elif len(sys.argv) == 3 :
  	main(sys.argv[1], sys.argv[2])
  else:
    print('Please provide PDF file path')
