import sys
import pandas as pd
import numpy as np
from pprint import pprint
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.pdfpage import PDFPage
from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams
from io import StringIO

def main(path):

  with open(path, 'rb') as f:
    rsrcmgr = PDFResourceManager()
    sio = StringIO()
    codec = 'utf-8'
    laparams = LAParams(char_margin = 20, line_margin=0.5, line_overlap=0.6, all_texts=True)
    device = TextConverter(rsrcmgr, sio, codec=codec, laparams=laparams)
    interpreter = PDFPageInterpreter(rsrcmgr, device)

    for page in PDFPage.get_pages(f):
        interpreter.process_page(page)
    f.close()

    raw_text = sio.getvalue()

    sio.close()
    device.close()


    reading_age = False
    features = []
    ages = []
    header = []
    page = [] # store ages within a page
    outlier = []
    hole = []
    page_num = 0

    data_idx = 1
    extract_result = {}

    raw_rows = raw_text.split('\n')
    print('Number of raw rows:', len(raw_rows))
    for idx, row in enumerate(raw_rows):
      row = row.strip()
      if 'Page' in row:
        page_num += 1
        if not reading_age:
          extract_result[data_idx] = {'p1': page_num, 'n1': len(page)}
          data_idx += 1
          page = []
      if idx == 2:
        header = row.split('  ')
      elif 'Age' in row:
        reading_age = True
        data_idx = 1
        idx_page = 0
      elif reading_age:
        if 'Page' in row: #end of page
          if len(page) and outlier != []:
            if len(outlier) > len(hole):
              hole = [0] + [h + 1 for h in hole]
              page = [''] + page
            if len(outlier) == len(hole):
              for o, h in zip(outlier, hole):
                page[h] = o
            else:
              print(outlier, hole)
              print(page_num)
              print(page)

          elif outlier == [] and hole != []: # real hole, no need to fill
            page.remove('')

          extract_result[data_idx]['p2'] = page_num
          extract_result[data_idx]['n2'] = len(page)
          data_idx += 1

          ages += page
          page = []
          outlier = []
          hole = []
          idx_page = 0
        else:
          if row.isdigit():
            if raw_rows[idx - 1].strip() == '' and raw_rows[idx + 1].strip() == '': #is outlier
              outlier.append(int(row))
            else:
              page.append(int(row))
              idx_page += 1
          else:
            if idx_page != 0 and row == '' and raw_rows[idx - 1].strip().isdigit() and raw_rows[idx + 1].isdigit() and raw_rows[idx + 2].isdigit(): #is hole
              page.append('')
              hole.append(idx_page)
              idx_page += 1
              
      else:
        row_split = row.split(' ')
        if len(row_split) > 2 and 'Page' not in row:
          page.append(row_split)
        if len(row_split) == 8:
          features.append(row_split)
          continue
          clean_row = []
          for idx2, item in enumerate(row_split):
            if idx2 == 0:
              clean_row.append(item)
            else:
              try:
                clean_row.append(float(item))
              except ValueError: #dirty row
                break
          else:
            features.append(tuple(clean_row))
          
    print('Number of clean feature rows:', len(features))
    print('Number of age rows:', len(ages))
    if len(ages) == len(features):
      print('Lengths match')
    else:
      print('Warning: Lengths dont match!')

    dataset = []
    for features, age in zip(features, ages):
      clean_row = []
      for idx, item in enumerate(features):
        if idx == 0:
          clean_row.append(item) #Sex
        else:
          try:
            clean_row.append(float(item))
          except ValueError:
            print(features)
            break
      else:
        clean_row.append(age)
        dataset.append(clean_row)
  

  header.append('Age')
  df = pd.DataFrame.from_records(dataset, columns=header)
  df.to_csv('data.csv')

  print('Done')
  # pprint(extract_result)

if __name__ == "__main__":
  if len(sys.argv) != 2:
    print('Please provide PDF file path')
    sys.exit(0)
  main(sys.argv[1])