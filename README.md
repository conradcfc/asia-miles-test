# Asia Miles Test by Conrad

##Files:
1.  data_extractor.py -> convert dirty pdf to csv.  e.g `python3 data_extractor.py data.pdf`
2.  model.py -> train + test + validate(optional) e.g `python3 model.py data.csv` or `python3 model.py data.csv data_validate.csv`
3.  data.csv -> example csv file extracted from dirty data.pdf

##Python Dependences:
1. sklearn
2. numpy
3. pandas
4. pdfminer

## Metrics
1. root mean square error
2. R-squared